#ifndef _TYPES_H_
#define _TYPES_H_

#include <netinet/in.h>
#include <netinet/tcp.h>

namespace remote { namespace protocols {

typedef uint32_t dbkey_t;

typedef uint8_t result_t;

typedef uint8_t status_t;

}}

#endif
