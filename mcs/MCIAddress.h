#ifndef MCIADDRESS_H_
#define MCIADDRESS_H_

namespace remote { namespace mcs {

/** Empty base class for Mote Control Infrastructure addresses. This class must be inherited
 * by any specific address types used by an infrastructure. **/
class MCIAddress
{
};

}}

#endif /*MCIADDRESS_H_*/
