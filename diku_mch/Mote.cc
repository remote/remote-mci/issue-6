#include "Mote.h"

namespace remote { namespace diku_mch {

using namespace remote::util;

Mote::Mote(std::string& p_mac, std::string& p_directory)
	: SerialControl(), mac(p_mac), directory(p_directory), isRunning(false)
{
	validate();
	if (isvalid && !setupTty(true))
		isvalid = false;

	Log::info("Mote %s (%s) @ %s", mac.c_str(), platform.c_str(), path.c_str());
}

bool Mote::setupTty(bool controlEnable)
{
	if (ttyData != ttyControl) {
		if (!openTty(FDTYPE_CONTROL, platform, ttyControl) || !openTty(FDTYPE_DATA, platform, ttyData))
			return false;

	} else if (!openTty(FDTYPE_DATA, platform, ttyData))
		return false;
	}

	if (controlEnable) {
		if (stop() == FAILURE)
			return false;
	}

	return true;
}


bool Mote::isValid()
{
	return isvalid;
}

void Mote::invalidate()
{
	isvalid = false;
}

void Mote::validate()
{
	isvalid = true;
	imagefile = directory + "image";

	binProgram = directory + "programmer";
	if (!File::exists(binProgram))
		isvalid = false;

	binControl = directory + "controller";
	if (!File::exists(binControl))
		binControl = "";

	path = File::readFile(directory + "path");
	if (path == "")
		isvalid = false;

	platform = File::readFile(directory + "platform");
	if (platform == "")
		isvalid = false;

	ttyData = File::readLink(directory + "tty/data");
	if (ttyData == "")
		isvalid = false;

	ttyControl = File::readLink(directory + "tty/control");
	if (ttyControl == "")
		isvalid = false;

	if (!isvalid)
		Log::warn("Mote %s is invalid", mac.c_str());
}


result_t Mote::start()
{
	return power("start");
}

result_t Mote::stop()
{
	return power("stop");
}

result_t Mote::reset()
{
	return power("reset");
}

result_t Mote::power(const std::string cmd)
{
	bool resetting = cmd == "reset";

	if (!isOpen(FDTYPE_CONTROL))
		return FAILURE;

	if (binControl != "") {

		std::string platform_env = "platform=" + platform;

		char * const args[] = {
			(char *) binControl.c_str(),
			(char *) ttyControl.c_str(),
			(char *) cmd.c_str(),
			NULL
		};
		char * const envp[] = {
			(char *) platform_env.c_str(),
			NULL
		};

		Log::info("Control mote %s to %s", mac.c_str(), cmd.c_str());
		Log::info("By running %s %s %s %s", binControl.c_str(), ttyControl.c_str(), cmd.c_str(), platform.c_str());

		if (runChild(args, envp)) {
			controlCmd = cmd;
			return SUCCESS;
		}

	} else if (!resetting || controlDTR(isRunning)) {
		bool enable = resetting ? !isRunning : cmd == "stop";

		if (controlDTR(enable)) {
			isRunning = !enable;
			return SUCCESS;
		}

		/* Mirror that the first reset DTR change succeeded. */
		if (resetting)
			isRunning = !isRunning;
	}

	Log::error("Failed to %s mote %s: %s",
		   cmd.c_str(), mac.c_str(), strerror(errno));
	closeTty(FDTYPE_CONTROL);
	return FAILURE;
}


result_t Mote::program(std::string tos, const uint8_t *image, uint32_t imagelen)
{
	if (hasChild())
		return FAILURE;

	if (File::writeFile(imagefile, image, imagelen)) {
		std::string mac_env = "macaddress=" + mac;
		std::string tos_env = "tosaddress=" + tos;
		std::string platform_env = "platform=" + platform;
		char * const args[] = {
			(char *) binProgram.c_str(),
			(char *) ttyControl.c_str(),
			(char *) imagefile.c_str(),
			NULL
		};
		char * const envp[] = {
			(char *) mac_env.c_str(),
			(char *) tos_env.c_str(),
			(char *) platform_env.c_str(),
			NULL
		};

		Log::info("Programming mote %s", mac.c_str());

		if (runChild(args, envp)) {
			controlCmd = "program";
			return SUCCESS;
		}

		remove(imagefile.c_str());
	}

	return FAILURE;
}

result_t Mote::cancelProgramming()
{
	bool success;

	if (controlCmd != "program")
		return FAILURE;

	success = endChild(true);
	remove(imagefile.c_str());
	setupTty(true);
	return success ? SUCCESS : FAILURE;
}

result_t Mote::getChildResult()
{
	bool success = endChild(false);

	if (success) {
		if (controlCmd == "program") {
			remove(imagefile.c_str());
			success = setupTty(true);
			return success ? SUCCESS : FAILURE;
		}
		else if (controlCmd == "start" || controlCmd == "reset")
			isRunning = true;
		else if (controlCmd == "stop")
			isRunning = false;

		success = setupTty(false);
	}

	Log::debug("getChildResult: success = %u", success);
	return success ? SUCCESS : FAILURE;
}


status_t Mote::getStatus()
{
	if (hasChild() && controlCmd == "program") {
		Log::debug("getStatus: programming");
		return MOTE_PROGRAMMING;
	}

	if (ttyControl != ttyData)
		if (!isOpen(FDTYPE_CONTROL) || !isOpen(FDTYPE_DATA)) {
			Log::debug("getStatus: unavailable");
			return MOTE_UNAVAILABLE;
		}
	else
		if (!isOpen(FDTYPE_CONTROL)) {
			Log::debug("getStatus: unavailable");
			return MOTE_UNAVAILABLE;
		}
	
	if (isRunning) {
		Log::debug("getStatus: running");
		return MOTE_RUNNING;
	}
	
	Log::debug("getStatus: stopped");
	return MOTE_STOPPED;
}

const std::string& Mote::getControlCommand()
{
	return controlCmd;
}


const std::string& Mote::getMac()
{
	return mac;
}

const std::string& Mote::getDevicePath()
{
	return path;
}

const std::string& Mote::getPlatform()
{
	return platform;
}

}}
