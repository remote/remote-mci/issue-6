#ifndef _SERIALCONTROL_H_
#define _SERIALCONTROL_H_

#include <string>
#include <errno.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ioctl.h>

#include "libutil/Log.h"
#include "types.h"

namespace remote { namespace diku_mch {

using namespace util;

enum FDTYPE
{
	FDTYPE_CONTROL,
	FDTYPE_DATA
};

class SerialControl
{
	public:
		SerialControl();
		~SerialControl();
		bool runChild(char * const args[], char * const envp[]);
		ssize_t readBuf(int fdtype, char *buf, size_t len);
		ssize_t writeBuf(int fdtype, const char *buf, size_t len);
		int getFd(int fdtype);
		int getFdActivity();
		void setFdActivity(int fdtype);
	protected:
		bool hasChild();
		bool isOpen(int fdtype);
		bool openTty(int fdtype, const std::string platform, const std::string tty);
		void closeTty(int fdtype);
		bool endChild(bool killChild);
		bool controlDTR(bool enable);
		int portData;
		int portControl;
		int fdActivity;
		pid_t childPid;
		struct termios oldsertiocontrol;
		struct termios oldsertiodata;
};

}}

#endif
