#include "SerialControl.h"

namespace remote { namespace diku_mch {

SerialControl::SerialControl()
	:portControl(-1), portData(-1), fdActivity(-1), childPid(-1)
{
}

SerialControl::~SerialControl()
{
	endChild(true);
	if (isOpen(FDTYPE_CONTROL))
		closeTty(FDTYPE_CONTROL);
	
	if (isOpen(FDTYPE_DATA))
		closeTty(FDTYPE_DATA);
}

bool SerialControl::openTty(int fdtype, const std::string platform, const std::string tty)
{
	struct termios newsertio;

	if (isOpen(fdtype)) {
		Log::error("TTY %s type %u already open", tty.c_str(), fdtype);
		return false;
	}

	/*
	 * 8 data, no parity, 1 stop bit. Ignore modem control lines. Enable
	 * receive. Set 38400 baud rate. NO HARDWARE FLOW CONTROL!
	 */
	if (platform == "dig582-2") {
		Log::debug("Platform %s", platform.c_str());
		newsertio.c_cflag = B38400 | CS8 | CLOCAL | CREAD;
	}
	else if(platform == "TMoteSky") {
		Log::debug("Platform %s", platform.c_str());
		newsertio.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
	}
	else if (platform == "MicaZ") {
		Log::debug("Platform %s", platform.c_str());
		newsertio.c_cflag = B57600 | CS8 | CLOCAL | CREAD;
	}

	/* Raw input. Ignore errors and breaks. */
	newsertio.c_iflag = IGNBRK | IGNPAR;

	/* Raw output. */
	newsertio.c_oflag = 0;

	/* No echo and no signals. */
	newsertio.c_lflag = 0;

	/* blocking read until 1 char arrives */
	newsertio.c_cc[VMIN]=1;
	newsertio.c_cc[VTIME]=0;

	switch (fdtype)
	{
		case FDTYPE_CONTROL:
			Log::debug("Opening control TTY %s", tty.c_str());
			portControl = open(tty.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
			Log::debug("TTY control opened %u", portControl);
			if (portControl == -1) {
				Log::error("Failed to open %s: %s", tty.c_str(), strerror(errno));
				return false;
			}
			tcgetattr(portControl, &oldsertiocontrol); /* save current port settings */
			tcflush(portControl, TCIFLUSH);
			tcsetattr(portControl, TCSANOW, &newsertio);
			break;
		case FDTYPE_DATA:
			Log::debug("Opening data TTY %s", tty.c_str());
			portData = open(tty.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
			Log::debug("TTY data opened %u", portData);
			if (portData == -1) {
				Log::error("Failed to open %s: %s", tty.c_str(), strerror(errno));
				return false;
			}
			tcgetattr(portData, &oldsertiodata); /* save current port settings */
			tcflush(portData, TCIFLUSH);
			tcsetattr(portData, TCSANOW, &newsertio);
			break;
		default:
			Log::error("Unknown type of port %u", fdtype);
			return false;
	}

	return true;
}

void SerialControl::closeTty(int fdtype)
{
	if (!isOpen(fdtype))
		return;
	
	switch (fdtype)
	{
		case FDTYPE_CONTROL:
			tcsetattr(portControl, TCSANOW, &oldsertiocontrol);
			close(portControl);
			portControl = -1;
			break;
		case FDTYPE_DATA:
			tcsetattr(portData, TCSANOW, &oldsertiodata);
			close(portData);
			portData = -1;
			break;
		default:
			Log::error("Unknown type of port %u", fdtype);
			return;
	}
}

bool SerialControl::runChild(char * const args[], char * const envp[])
{
	int pfd[2];

	if (pipe(pfd) < 0)
		return false;
	closeTty(FDTYPE_CONTROL);
	closeTty(FDTYPE_DATA);

	if ((childPid = fork())) {
		/* Only use the reader end if we forked. */
		if (hasChild()) 
			portControl = pfd[0];
		else
			close(pfd[0]);
		close(pfd[1]);

	} else {
		/* Redirect all standard output to the parent's pipe. */
		if (dup2(pfd[1], STDOUT_FILENO) != -1 &&
		    dup2(pfd[1], STDERR_FILENO) != -1) {
		        close(pfd[0]);
			close(pfd[1]);
			execve(args[0], args, envp);
		}
		/* XXX: Make the failed child exit immediately. */
		_exit(EXIT_FAILURE);
	}

	return hasChild();
}

bool SerialControl::endChild(bool killChild)
{
	int status;

	if (!hasChild())
		return false;

	if (killChild)
		kill(childPid, SIGKILL);

	waitpid(childPid, &status, 0);
	close(portControl);
	portControl = childPid = -1;

	return killChild || (WIFEXITED(status) && WEXITSTATUS(status) == 0);
}

bool SerialControl::controlDTR(bool enable)
{
	int tmp = TIOCM_DTR;
	int req = enable ? TIOCMBIS : TIOCMBIC;

	return ioctl(portControl, req, &tmp) != -1;
}

ssize_t SerialControl::readBuf(int fdtype, char *buf, size_t len)
{
	ssize_t res = 0;

	switch (fdtype)
	{
		case FDTYPE_CONTROL:
			res = read(portControl, buf, len);
			break;
		case FDTYPE_DATA:
			res = read(portData, buf, len);
			break;
		default:
			Log::error("Unknown type of port %u", fdtype);
			break;
	}

	if (res <= 0 && !hasChild()) {
		Log::error("Error while reading from TTY type=%u, closing it", fdtype);
		closeTty(fdtype);
	}

	return res;
}

ssize_t SerialControl::writeBuf(int fdtype, const char* buf, size_t len)
{
	switch (fdtype)
	{
		case FDTYPE_CONTROL:
			return write(portControl, buf, len);
			break;
		case FDTYPE_DATA:
			return write(portData, buf, len);
			break;
		default:
			Log::error("Unknown type of port %u", fdtype);
			return 0;
	}
}

bool SerialControl::hasChild()
{
	return childPid != -1;
}

bool SerialControl::isOpen(int fdtype)
{
	switch (fdtype)
	{
		case FDTYPE_CONTROL:
			return portControl != -1 && !hasChild();
			break;
		case FDTYPE_DATA:
			return portData != -1 && !hasChild();
			break;
		default:
			Log::error("Unknown type of port %u", fdtype);
			return false;
	}
}

int SerialControl::getFd(int fdtype)
{
	switch (fdtype)
	{
		case FDTYPE_CONTROL:
			return portControl;
			break;
		case FDTYPE_DATA:
			return portData;
			break;
		default:
			Log::error("Unknown type of port %u", fdtype);
			return -1;
	}
}

void SerialControl::setFdActivity(int fdtype)
{
	fdActivity=fdtype;
}

int SerialControl::getFdActivity()
{
	return fdActivity;
}

}}
